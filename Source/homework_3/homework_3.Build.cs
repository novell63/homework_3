// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class homework_3 : ModuleRules
{
	public homework_3(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
