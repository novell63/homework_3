// Copyright Epic Games, Inc. All Rights Reserved.

#include "homework_3GameMode.h"
#include "homework_3HUD.h"
#include "homework_3Character.h"
#include "UObject/ConstructorHelpers.h"

Ahomework_3GameMode::Ahomework_3GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Ahomework_3HUD::StaticClass();
}
