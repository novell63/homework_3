// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "homework_3GameMode.generated.h"

UCLASS(minimalapi)
class Ahomework_3GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Ahomework_3GameMode();
};



